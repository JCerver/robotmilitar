                                      //DEFINICIÓN DE VARIABLES A UTILIZAR 
#define ENA   14          // Habilitar / acelerar los motores a la derecha        GPIO14(D5)
#define ENB   12          // Habilitar / acelerar los motores a la izquierda        GPIO12(D6)
#define IN_1  15          // L298N in1 motor a la derecha           GPIO15(D8)
#define IN_2  13          // L298N in2 motors a la derecha           GPIO13(D7)
#define IN_3  2           // L298N in3 motor a la izquierda            GPIO2(D4)
#define IN_4  0           // L298N in4 motor a la izquierda            GPIO0(D3)

#include <ESP8266WiFi.h>        //Se incluyen las librerias necesarias para reconocimiento del modulo NodeMCU
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <SoftwareSerial.h>

boolean canMove = true;
String sensoresCadena = ",,";
String command;             //String para almacenar el estado de la aplicación.
int speedCar = 800;         // Velocidad inicial de movimiento, va de un rango de 400 - 1023.
int speed_Coeff = 3;
int c = 0;

const char* ssid = "Akatsuki2";       //Definición de la red a conectarse
const char* password = "itachi123";   //Contraseña para acceder y comunicarse con el router.
ESP8266WebServer server(80);
SoftwareSerial ArduinoUNO(5, 4);      //Definición de puertos que seran utilizados como seriales en el modulo NodeMCU 
                                      //Correspondiente a los pines D1 y D2


byte BufferIn[4];
boolean StringCompleta = false;                           //variable booleana para saber cuando se recibio el string de movimientos completo
boolean ValPWMLeido = false;

byte Cont_char = 0;                                        //variable usada para saber si se recibieron los datos necesarios desde el Serial


void setup() {
  ArduinoUNO.begin(9600);             //Inicialización de los seriales 
  Serial.begin(9600);

  pinMode(ENA, OUTPUT);              // //Se especifican los puertos correspondientes de salida     
  pinMode(ENB, OUTPUT);
  pinMode(IN_1, OUTPUT);
  pinMode(IN_2, OUTPUT);
  pinMode(IN_3, OUTPUT);
  pinMode(IN_4, OUTPUT);
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);             //Esta inicial de los LED traseros del carrito



  // Connecting WiFi

  WiFi.begin(ssid, password);       //Inicialización de la comunicación con la red a traves del protocolo IEEE 802.11 b/g/n Wi-Fi. 

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {       //Condicional para validar el estado de la conexión
    Serial.print(".");
    delay(500);
  }
  delay(500);
  Serial.println("");
  delay(500);
  Serial.println("WiFi connected");             
  delay(500);
  Serial.println(WiFi.localIP());
  delay(500);
  Serial.println(WiFi.localIP());

  server.on("/", []() {

    command = server.arg("State");
    Serial.println(command);

    if (canMove) {                                     //Condicionales correspondientes al monitoreo de los movimientos de la aplicación    
      if (command == "F") goAhead();                   //Dependiendo del valor recibido a traves de la red, será el movimiento a ejecutar, los condcionales
      else if (command == "B") goBack();               //Mandan a llamar a los metodos correspondientes para iniciar el movimiento. 
      else if (command == "L") goLeft();                  
      else if (command == "R") goRight();
      else if (command == "I") goAheadRight();
      else if (command == "G") goAheadLeft();
      else if (command == "J") goBackRight();
      else if (command == "H") goBackLeft();
      else if (command == "0") speedCar = 400;        //A traves del valor recibido del scroll integrado en la aplicación 
      else if (command == "1") speedCar = 470;        //será la velocidad con la que el robot ejecute sus movimientos 
      else if (command == "2") speedCar = 540;
      else if (command == "3") speedCar = 610;
      else if (command == "4") speedCar = 680;
      else if (command == "5") speedCar = 750;
      else if (command == "6") speedCar = 820;
      else if (command == "7") speedCar = 890;
      else if (command == "8") speedCar = 960;
      else if (command == "9") speedCar = 1023;
      
    }
    if (command == "S") stopRobot();                 //Al recibir el valor "S" se realiza un  paro de movimiento en el robot, dejandolo en la posición en que se encuentra
    else  if (command == "X") sensores();            //Al recibir el valor "X" (enviado desde la aplicación cada 6 seg.) se realiza el cálculo correspondiente a cada uno de los 3 
                                                     //sensores integrados.   
    server.send(200, "text/html", sensoresCadena);
    delay(10);
  });

  server.begin();
}


void goAhead() {                //Método encargado de mover el carrito hacia enfrente 
     
  digitalWrite(IN_1, LOW);      //Combinación correspondiente del motor derecho para realizar el movimiento
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar);   //Se determina la velocidad a ejecutar el movimiento hacia la derecha dependiendo del valor recibido del scroll integrado en la aplicación 

  digitalWrite(IN_3, LOW);       //Combinación correspondiente del motor izquierdo para realizar el movimiento
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar);    //Se determina la velocidad a ejecutar el movimiento hacia la izquierda dependiendo del valor recibido del scroll integrado en la aplicación 

  digitalWrite(16, HIGH);        //Al ejecutar el movimiento enciende los LED traseros
}

void goBack() {                //Método encargado de mover el carrito hacia atrás

  digitalWrite(IN_1, HIGH);      //Combinación correspondiente del motor derecho para realizar el movimiento
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);    //Se determina la velocidad a ejecutar el movimiento hacia la derecha dependiendo del valor recibido del scroll integrado en la aplicación 

  digitalWrite(IN_3, HIGH);      //Combinación correspondiente del motor izquierdo para realizar el movimiento
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);    //Se determina la velocidad a ejecutar el movimiento hacia la izquierda dependiendo del valor recibido del scroll integrado en la aplicación 

  digitalWrite(16, HIGH);        //Al ejecutar el movimiento enciende los LED traseros
}

             /*NOTA: los siguientes métodos conllevan a la misma descripción explicada anteriormente, las variables IN_1 y IN_2 corresponden al estado del motor derecho, IN_3 y IN_4 
                     correponden al estado del motor izquierdo, ENA es la velocidad para el motor derecho, mientras que ENB correponde a la velocidad del motor izquierdo para ejecutar
                     los movimientos correpondientes a los métodos.*/ 
                        
void goRight() {               //Método encargado de mover el carrito hacia el lado derecho

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar);

  digitalWrite(16, HIGH);
}

void goLeft() {                 //Método encargado de mover el carrito hacia el lado izquierdo

  //ArduinoUNO.write('q');
  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);

  digitalWrite(16, HIGH);
}

void goAheadRight() {            //Método encargado de mover el carrito hacia de manera diagonal-derecha hacia enfrente

  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar / speed_Coeff);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar);

  digitalWrite(16, HIGH);
}

void goAheadLeft() {             //Método encargado de mover el carrito hacia de manera diagonal-izquierda hacia enfrente

  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, HIGH);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, HIGH);
  analogWrite(ENB, speedCar / speed_Coeff);

  digitalWrite(16, HIGH);
}

void goBackRight() {             //Método encargado de mover el carrito hacia de manera diagonal-derecha hacia atrás

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar / speed_Coeff);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);

  digitalWrite(16, HIGH);
}

void goBackLeft() {            //Método encargado de mover el carrito hacia de manera diagonal-izquierda hacia atrás

  digitalWrite(IN_1, HIGH);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, HIGH);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar / speed_Coeff);

  digitalWrite(16, HIGH);
}

void stopRobot() {             //Método encargado de detener el carrito

  digitalWrite(IN_1, LOW);
  digitalWrite(IN_2, LOW);
  analogWrite(ENA, speedCar);

  digitalWrite(IN_3, LOW);
  digitalWrite(IN_4, LOW);
  analogWrite(ENB, speedCar);

  digitalWrite(16, LOW);
}

void loop() {

  server.handleClient();          //Obtener un cliente o solicitud

  if (ArduinoUNO.available()) {         //Condicionales para verificar si la comunicación serial con el arduino UNO esta disponible
    if (ArduinoUNO.available() > 0) {   //Verficación de recibir datos por parte del arduino UNO

      int valor = ArduinoUNO.read();                   //Se obtiene el valor recibido del arduino UNO en la comunicación serial
      String cadena = ArduinoUNO.readString();         //Se obtiene la concatenación de valores recibidos del arduino UNO
      ArduinoUNO.flush();

      Serial.println(valor);
      Serial.println(cadena);

      if (valor == 200) {                                 //si se recibio un valor de 200, corresponde a una solicitud para determinar el valor calculado del sensor de luminosidad del arduino UNO

        Serial.println("La luminosidad es: ");
        Serial.println(cadena);
      } else if (valor == 201) {                        //si se recibio un valor de 201, corresponde a una solicitud para determinar el valor calculado del sensor de temperatura del arduino UNO

        Serial.println("La temperatura es: ");
        Serial.println(cadena);

      } else if (valor == 202) {                         //si se recibio un valor de 202, corresponde a una solicitud para determinar el estado del sensor de obstaculos infrarrojo
        
        if (cadena == "1") {                             //Al no presentarse un obstaculo devuelve el estado o valor de 1
          Serial.println("No");
          canMove = true;                                //Permite moverse en cualquier dirección.
        } else {
          Serial.println("Sí");                          //Al  presentarse un obstaculo devuelve el estado o valor de 0
          stopRobot();
          canMove = false;                               // No permite en movimiento hacia dirección de enfrente.
          
        }
      } else if (valor == 205) {                        
        sensoresCadena = cadena;

      }

    }
  }

}

void HTTP_handleRoot(void) {

  if ( server.hasArg("State") ) {
    Serial.println(server.arg("State"));
  }
  server.send ( 200, "text/html", sensoresCadena );
  delay(1);
}


void pinPrueba(int cantidad) {             //Método para realizar pruebas en la comunicación serial entre ambos microcontroladores (ARDUINO UNO y NodeMCU)

  for (int i = 0; i < cantidad; i++ ) {
    digitalWrite(2, HIGH);
    delay(100);
    digitalWrite(2, LOW);
    delay(100);
  }

}

void sensores() {                          //Método correspondiente para obtener valor de los sensores
  
  ArduinoUNO.write("205");                //Se envia el identificador 205 al Arduino UNO, al recibir este valor, el arduino UNO realiza el cálculo necesario para determinar los valores y estados de los
                                          // 3 sensores integrados (luminosidad, temperatura y humedad, detector de obstáculos). 
  delay(1000);
  

  if (ArduinoUNO.available()) {           //Verificación de conexión serial con Arduino UNO
    if (ArduinoUNO.available() > 0) {     //Verificar si recibe datos por parte del Arduino UNO
      int a = ArduinoUNO.read();              
      sensoresCadena = ArduinoUNO.readString();   //El arduino UNO envia los 3 valores de los sensores concatenados en una sola cadena, y es leido por el modulo NodeMCU
    }
  }

}
