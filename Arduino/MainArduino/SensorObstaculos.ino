void setupSensorObstaculos() {
  pinMode(SENSOR_OBSTACULO  , INPUT);                                         //definir pin como entrada
}

void verificarYEnviarEstadoSensorObstaculo() {
  /*
    Este método lee el estado de sensor de obstculos y se envia respuesta al módulo de wifi por medio
    de la comunicación serial que conecta al Arduino UNO con el módulo Wifi NodeMCU
  */
  verificarEstadoSensorObstaculo();                                 //primero lee si hay agún obstaculo y guarda valor leido

  wifiserial.write(202);                                            //Se envia un 202 para que el ModeMCU Wifi conozca que valor va a recibir
  wifiserial.print(opcionObstaculo);
  Serial.print(opcionObstaculo);
}

void verificarEstadoSensorObstaculo() {
  /*
    Este método verifica si existe algun obstaculo y lo almacena en la variable opcionObstaculo
  */
  int value = 0;
  value = digitalRead(SENSOR_OBSTACULO);                            //lectura digital de pin
  opcionObstaculo = "";

  if (value == HIGH) {                                              //No hay ningún obstaculo
    opcionObstaculo = "0";
  } else {                                                          //Hay algún obstaculo
    opcionObstaculo = "1";

  }
}

void verificarSiHayObstaculo() {
  /*
    Este método lee el estado de sensor de obstculos y se envia respuesta al módulo de wifi por medio
    de la comunicación serial que conecta al Arduino UNO con el módulo Wifi NodeMCU,
    Pero solo envia la informacion por el Serial si el valor cambio, es decir solo cuando detecta que hay un obstaculo y cuando
    el obstaculo se fue. Como este método esta en el loop() con este método se evita que se envien siempre datos por el Serial
    (por ejemplo solo s envia la informacion cuando se cambia de '1' a '0'  o cuando se cambia de '0' a '1')
  */

  int value = 0;
  value = digitalRead(SENSOR_OBSTACULO);  //lectura digital de pin

  if (value == HIGH) {                                              //No hay obstaculo
    if (opcionObstaculo.equals("1")) {                              //Solo si antes habia un obstaculo y se detecto que no lo hay mas se envia la opcion
      wifiserial.write(202);                                        //Se envia un 202 para que el ModeMCU Wifi conozca que valor va a recibir
      wifiserial.print(opcionObstaculo);
      //Serial.println(opcionObstaculo);
    }

    //Si ya no hay obstaculos ya no vuelve a enviar que no hay obstaculos, solo cuando se requiere, pero
    //aun almacena que no hay ningun obstaculo
    opcionObstaculo = "0";
  } else {                                                          //Hay obstaculo
    if (opcionObstaculo.equals("0")) {                              //Solo si antes no habia un obstaculo y se detecto que ahora lo hay se envia la opcion
      wifiserial.write(202);                                        //Se envia un 202 para que el ModeMCU Wifi conozca que valor va a recibir
      wifiserial.print(opcionObstaculo);
      //Serial.println(opcionObstaculo);
    }

    //Si hay obstaculos ya no vuelve a enviar que hay obstaculos, solo cuando se requiere, pero
    //aun almacena que hay ningun obstaculo
    opcionObstaculo = "1";
  }
  delay(60);                                                        //Un breve retraso para que envie los datos correctamente por el serial
}
