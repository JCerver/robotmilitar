#include <SoftwareSerial.h>                                                 //anadir la libreria para poder usar pines digitles como seriales RX,TX
#include <DHT_U.h>
#include <DHT.h>                                                            //anadir la libreria para gestionar un display LCD


/************************************************
  Programa: Sistema robot militar

  Autores:
  José Guadalupe de Jesús Cerera Barbosa.
  Miguel Ángel Ramírez Lira.
  Alfredo Valdivia Barajas.

  El programa que a continuación se muestra es el que va cargado en el Arduino UNO, a grandes rasgos este programa permite recibir solcitudes para conocer
  valores de los sensores de humedad,temperatura y obstaculos  y dar respuestas por medio de comunicacón serial. la cual comunica a un Arduino UNO y a
  un módulo de Wifi NodeMCU
 ***********************************************/

SoftwareSerial wifiserial (10, 11);                                         //Pines digitales del DodeMCU que serán usados para enviar y recibir datos del arduino UNO, pin 10=RX, 11=TX



/*-----------------------------------------------------------          1 PARA USO DEL SERSOR DE LUMINOSIDAD               */
/****************               CONSTANTES                     *******************/
const long A = 5000;                                                        //Resistencia en oscuridad en KΩ
const int B = 15;                                                           //Resistencia a la luz (10 Lux) en KΩ
const int RC = 10;                                                          //Resistencia calibracion en KΩ
const int LDR_SENSOR = A0;                                                  //Pin conectado a la salida del sensor LDR

/****************               VARIABLES         *******************/
int V;                                                                      //voltaje obtenido del sensor LDR
int ilum;

/*-----------------------------------------------------------          2 PARA USO DEL SERSOR DE OBSTACULOS               */
/****************               CONSTANTES                     *******************/
#define SENSOR_OBSTACULO 3                                                  //pin de entrada digital donde se conecta el detector de obstaculos

/****************               VARIABLES         *******************/
String opcionObstaculo = "";                                                //variable que permite guardar un '0' cuando no ningun obstaculo y un '1' cuando hay un obstaculo

/*-----------------------------------------------------------          3 PARA USO DEL SERSOR DE HUMEDAD               */
/****************               CONSTANTES                     *******************/
#define DHTPIN 9                                                            //Pin conectado al sensor de humedad DTH11 con PCB 
#define DHTTYPE DHT11                                                       //Indicamos el tipo de sensor DTH usado (Se usará el DTH11)

/****************               VARIABLES         *******************/
float valorHumedad = 0;                                                     //variable para almacenar el valor de la humedad devuelto por el DHT11
float valorTemperaturaCentrigrados = 0;                                     //variable para almacenar el valor de la temperatura en grados Centigrados devuelto por el DHT11
float valorTemperaturaFahrenheit = 0;                                       //variable para almacenar el valor de la temperatura en grados Fahrenheit devuelto por el DHT11

/*************                Inicializar Sensor DTH11          *******************/
DHT dht(DHTPIN, DHTTYPE);



void setup() {
  Serial.begin(9600);                                                       // Inicializamos comunicación serie
  wifiserial.begin(9600);                                                   // Inicializamos comunicación serie con el NodeMCU

  pinMode(13, OUTPUT);                                                      //Pin usado para conectar un LED que simula faro trasero
  pinMode(4, OUTPUT);                                                       //Pin usado para conectar un LED que simula faro trasero
  pinMode(5, OUTPUT);                                                       //Pin usado para conectar un LED que simula faro delantero
  digitalWrite(13, LOW);                                                    //Se manda LOW para que los LEDs inicien apagados
  digitalWrite(4, LOW);                                                     //Se manda LOW para que los LEDs inicien apagados
  digitalWrite(5, LOW);                                                     //Se manda LOW para que los LEDs inicien apagados


  setupSensorObstaculos();
  setupSensorHumedad();
}

void loop() {
  verificarSiHayObstaculo();                                                //Método que verifica y avisa al NodeMCU si hay algun obstaculo

  /*Verificar datos que solicita módulo de wifi NodeMCU por medio de la comunicacion Serial*/
  if (wifiserial.available())                                               //En esta parte se verifica es es lo que está solicitando el módulo de Wifi
  {
    if (wifiserial.available() > 0)                                         //Solo si se recie algo del módulo de wifi entra a este if
    {
      //pinPrueba(3);
      String valor = wifiserial.readString();                               //Guarda la opcion solicitada desde el NodeMCU, ya sea '200','201','202','203'
      wifiserial.flush();
      if (valor == "200") {                                                 //si se recibio un 200, el NodeMCU esta solicitando a este arduino la luminosidad
        //pinPrueba(5);                                                     //Solo se probaba que el pin de prueba encendiera N veces si entro a este if
        calcularYEnviarLuminosidad();
      } else if (valor == "201") {                                          //si se recibio un 201, el NodeMCU esta solicitando a este arduino la temperatura
        //pinPrueba(6);                                                     //Solo se probaba que el pin de prueba encendiera N veces si entro a este if
        calcularTemperatura();
      } else if (valor == "202") {                                          //si se recibio un 202, el NodeMCU esta solicitando a este arduino la confirmacion de si existe algun obstaculo
        verificarYEnviarEstadoSensorObstaculo();
        //pinPrueba(7);                                                     //Solo se probaba que el pin de prueba encendiera N veces si entro a este if
      } else if (valor == "205") {                                          //si se recibio un 205, el NodeMCU esta solicitando a este arduino todos los datos juntos de los sensores
        enviarDatosSensores();
        //pinPrueba(8);                                                     //Solo se probaba que el pin de prueba encendiera N veces si entro a este if
      }

    }
  }

  encenderFaros();                                                          //Se dirige al método que verifica si es necesario encender los LED
}

void pinPrueba(int cont) {
  /*
    Este método se usa solo para probar que realmente se entra a las opciones del if dentro del método loop,
    se enciende el led del arduino N cantidad de veces dependiendo de lo que reciba este método
  */
  for (int i = 0; i < cont; i++) {
    digitalWrite(13, HIGH);
    delay(600);
    digitalWrite(13, LOW);
    delay(600);
  }
}

void enviarDatosSensores() {
  /*
    Este método calcula los valores leidos del sensor de Luminosidad, Humedad (del cual obtenemos solo la temperatura) y el sensor de obstaculos,
    una vez que cálcula los valores de los sensores, concatena los resultados separados por comas, siendo el primer valor la luminosidad
    el segundo valor la temperatura y por último un '1' o un '0' el cual nos indica si hay un obstaculo=1,si no hay obstaculo=0
  */
  
  //-----------------------------------------------------------------------Primero se capturan datos de los sensores
  calcularLuminosidad();                                                   //Almacena el dato de la iluminacion captada por el sensor de Luminosidad
  leerHumedadyTemperatura();                                               //Almacena el dato de la temperatura captada por el sensor de Temperatura
  verificarEstadoSensorObstaculo();                                        //Almacena '1' o '0' que indica si hay algun obstaculo o no, captado por el sensor de obstaculos

  String valores = "";
  
  //-----------------------------------------------------------------------Se concatenan todos los valores de iluminacion, temperatura y obstaculo separados por comas
  //-----------------------------------------------------------------------Si se separa por comas es mas fácil saber donde inicia y termina un valor recibido
  valores += ilum;
  valores += ",";
  valores += valorTemperaturaCentrigrados;
  valores += ",";
  valores += opcionObstaculo;


  wifiserial.write(205);                                                  //Se envia un 205 para que el ModeMCU Wifi conozca que va a recibir todos los valores de los 3 sensores
  wifiserial.print(valores);                                              //Se envia la cadena que contiene los 3 valores de los 3 sensores
  //Serial.println(valores);
}

