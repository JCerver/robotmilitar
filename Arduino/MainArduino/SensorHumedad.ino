void setupSensorHumedad() {
  dht.begin();                                                      // Comenzamos el sensor DHT
}

void calcularTemperatura() {
  /*
    Este método lee la temepratura y se la envia al módulo de wifi por medio de la comunicaci+on serial que conecta al Arduino UNO con el módulo Wifi NodeMCU
  */
  leerHumedadyTemperatura();                                        //Primero se guardan los datos leidos del DHT11
  wifiserial.write(201);                                            //Se envia un 201 para que el ModeMCU Wifi conozca que valor va a recibir
  wifiserial.print(valorTemperaturaCentrigrados);                   //Se envia el valor de la temperatura al módulo de Wifi
  Serial.print(valorTemperaturaCentrigrados);
}

void leerHumedadyTemperatura() {
  /*
    Este método lee la humedad y la temperatura expresada en grados Centigrados o grados Fahrenheit,
    los valores se almacenan en las variables correspondientes
  */
  valorHumedad = dht.readHumidity();                                //leer la humedad relativa
  valorTemperaturaCentrigrados = dht.readTemperature();             //leer la temperatura en grados centígrados (por defecto)
  valorTemperaturaFahrenheit = dht.readTemperature(true);           //Leer la temperatura en grados Fahrenheit
}


