void calcularYEnviarLuminosidad() {
  /*
    Esté método es utilizado cuando el módulo de wifi NodeMCU solicita la luminosidad y solo se envia la respuesta del valor de iluminacion ,
    Se usa la comunicacion serial que comunica al Arduin Uno con el Sensor de wifi NodeMCU
  */

  calcularLuminosidad();                                                      //Primero se leen los datos leidos por el sensor de iluminacion
  wifiserial.write(200);                                                      //Se envia un 200 para que el ModeMCU Wifi conozca que valor va a recibir
  wifiserial.print(ilum);                                                     //Se envia el valor de la iluminacion al módulo de Wifi
}

void calcularLuminosidad() {
  /*
    Este metodo solo se utiliza para guardar en las variables de luminosidad los datos leidos del sensor DHT11
  */

  V = analogRead(LDR_SENSOR);                                                 //voltaje obtenido del sensor
  ilum = ((long)V * A * 10) / ((long)B * RC * (1024 - V));                    //usar si LDR entre A0 y Vcc (Depende del tipo de conexión)
}

void encenderFaros() {
  /*
    Esté método permite encender los LED que simulan faros en el coche inalambrico solo cuando hay poca iluminación
  */

  calcularLuminosidad();                                                      //Primero se leen los datos leidos por el sensor de iluminacion

  digitalWrite(4, LOW);                                                       //Los LED siempre estan apagados
  digitalWrite(5, LOW);

  if (ilum < 10) {                                                            //Solo si hay poca iluminación se enciendenlos LED
    digitalWrite(4, HIGH);
    digitalWrite(5, HIGH);
  }
}

