<!-- Se inicia un archivo HTML estándar-->
<!DOCTYPE html>
<html>
<head>
<!-- En el encabezado de la página web está la configuracion básica para un contenido responsivo-->
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<!-- El cuerpo de la página web principla es color verde-->
<body style="background-color:MediumSeaGreen;">

<script>
        //Función para ejecutar la función de refrescar el iframe cada 6 segundos
        window.setInterval(function(){
                reloadFrame()
        }, 6000);

        //Función para recargar la página interna "iframeData"
        function reloadFrame(){
                console.log("refrescando");
                document.getElementById("iframeData").contentWindow.location.reload();
        }

        //Funcion que envía datos por medio del protocolo HTTP con el método GET 
        function sendData(opt){
       
                var xhr = new XMLHttpRequest(); //Se inicia variable de petición de datos
                xhr.open('GET', 'http://192.168.1.246/?State=' + opt, true); //Se prepara la petición por el método GET a la dirección del ESP8266 agregando una variable State
                xhr.timeout = 6000; // Tiempo en milisegundos para que la petición tenga una vigencia en caso de que no se encuentre la dirección IP
                xhr.send();     //Realiza la petición HTTP
        }

</script>
        <!-- Bloque principal que contiene un borde con diseño que rodea el contenido de la página-->
    <div style="margin:5px;padding: 20px; border:50px ridge DARKORANGE; border-radius: 100px; width:700px;">
        <!-- Tabla que contiene a los botones de dirección. Cada botón tiene un evento que llama al método sendData() 
        el cual envía al ESP8266 un commando correspondiente al movimiento-->
    <table style='margin:auto'>
        <tbody>
        <tr>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('G')" onmouseup="sendData('S')" ><img src="imagenes/flecha1.png"  height="90" width="90"></button>
        </td>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('F')" onmouseup="sendData('S')" ><img src="imagenes/flecha2.png"  height="90" width="90"></button>
        </td>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('I')" onmouseup="sendData('S')" ><img src="imagenes/flecha3.png"  height="90" width="90"></button>
        </td>
        </tr>
        <tr>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('L')" onmouseup="sendData('S')" ><img src="imagenes/flecha8.png"  height="90" width="90"></button>
        </td>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('S')" onmouseup="sendData('S')" ><img src="imagenes/circulo.png"  height="90" width="90"></button>
        </td>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('R')" onmouseup="sendData('S')" ><img src="imagenes/flecha4.png"  height="90" width="90"></button>
        </td>
        </tr>
        <tr>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('H')" onmouseup="sendData('S')" ><img src="imagenes/flecha7.png"  height="90" width="90"></button>
        </td>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('B')" onmouseup="sendData('S')" ><img src="imagenes/flecha6.png"  height="90" width="90"></button>
        </td>
        <td style="width: 139px; text-align: center;">
                <button  onmousedown="sendData('J')" onmouseup="sendData('S')" ><img src="imagenes/flecha5.png"  height="90" width="90"></button>
        </td>
        </tr>
        </tbody>
        </table>
                <!-- Pagina interna que será la que se actualizará cada 6 segundos mostrando las estadísticas de loos sensores-->
        <p align="center"><iframe id="iframeData" src="iframeData.php" name="iframe" width="700px" height="200px"></iframe></p>
        
        </div>
        

</body>
</html>