<!-- Se inicia un archivo HTML estándar-->
</<!DOCTYPE html>
<html>
<head>
<script>
    
        function sendData(opt){
        
                var xhr = new XMLHttpRequest(); //Se inicia variable de petición de datos
                xhr.open('GET', 'http://192.168.1.246/?State=' + opt, false); //Se prepara la petición por el método GET a la dirección del ESP8266 agregando una variable State
                xhr.send();     //Se envía la petición web or HTTP
                
                if (xhr.status == 200){ //En caso de que sea exitosa la conexión se ejecuta la actualización de las estadísticas de los sensores
                
                        var cadena = xhr.responseText;  //Cadena que guarda la respuesta de ESP8266 en texto.
                        var array = cadena.split(',');  //Los datos de la respuesta siempre osn 3 y están separados por comas, por lo que s ehace un SPLIT para separarlos.
                        document.getElementById("textTemperature").innerHTML = "Luminosidad:" + array[0] + " lx";       //Se actualiza la luminosidad
                        document.getElementById("textLuminosidad").innerHTML = "Temperatura: " + array[1] + " °";       //Se actualiza la temperatura
                        document.getElementById("textObjetos").innerHTML = "Objetos encontrados: " + array[2];          //Se actualiza la etiqueta que detecta objetos
                        if(array[2] == "1"){    //Se toma la decisión para poder cambiar el color de l etiqueta de objetos detectados
                                document.getElementById("textObjetos").style.color = "#FF0000"; //En color rojo si se detectó objeto
                        }else{
                                document.getElementById("textObjetos").style.color = "#FFFFFF"; //En color blanco si no se detectó objeto
                        }
                }
        }
</script>

<!-- Se inserta un pequeño bloque de diseño CSS para el bloque de las etiquetas de estadísticas en donde se centra el mismo-->
<style>
        .center{
                width: 500px;
                margin-left:auto;
                margin-right:auto;
        }

</style>
</head>
<body>
<!-- Se crean 3 etiquetas para mostrar la información de los sensores-->
<div class="center">
                <b><h2 style='text-align:left' id="textTemperature">Temperatura:</h2></b>
                <b><h2 style='text-align:left' id="textLuminosidad">Luminosidad:</h2></b>
                <b><h2 style='text-align:left' id=textObjetos>Objetos detectados:</h2></b>
        </div>


</body>
<script>
        //Una vez cargada la página se solicita la actualización de los datos de los sensores enviando el comando X a ESP8266
        sendData('X');
</script>
</html>