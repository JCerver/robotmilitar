# RobotMilitar

![Portada](Imagenes/vista_lateral_der.jpg)

![Portada](Imagenes/vista_lateral_izq.jpg)

## Descripción
El sistema permite controlar un robot móvil de control remoto a traves de la tecnologia WI-FI. EL robot esta diseñados para aplicaciones militares, desde el transporte hasta la búsqueda y exploración.



## Datos de los Alumnos
**Instituto Tecnológico de León** 

**CARRERA:** Ingeniería en Sistemas Computacionales

**Practica Final: Arduino Networking**

**Materia:** Sistemas Programables

**Profesor:** Ing. Levy Rojas Carlos Rafael

**Integrantes:** 

* José Guadalupe de Jesús Cervera Barbosa.

* Miguel Ángel Ramírez Lira. 

* Alfredo Validivia Barajas. 

***

# Prerrequisitos
## Programas necesarios📋


*    El Arduino IDE el cual se puede descargar de la pagina oficial: [Pagina de Arduino](https://www.arduino.cc/en/Main/Software)  
*    Git ya sea en linux o windows en caso de querer clonar el repositorio.
*    XAMPP: paquete de software libre, que consiste principalmente en el sistema de gestión de bases de datos MySQL, el servidor web Apache y los intérpretes para lenguajes de script PHP y Perl.


### Instalación de GIT
para instalar git solo se tiene que abrir una terminal e introducir el siguiente comando
```
$ apt-get install git
```


## Materiales: 🔧

*    1 Protoboard.
*    1 Arduino UNO (o alguno equivalente).
*    1 Modulo WI-FI NodeMCU
*    1 sensor o fotoresistencia LDR (GL55)
*    1 sensor DHT11 para temperatura y humedad
*    1 Detector de obstáculos con sensor infrarojo
*    Cables 
*    1 x Chasis de carro en acrílico transparente 21.3cm x 15.2cm
*    2 x motor de engranajes (1:48)
*    2 x llantas
*    1 x Interruptor de apagado/encendido
*    1 x Caja para 4 baterías AA (Las baterías no están incluidas)
*    Tornillos, tuercas y separadores metálicos
*    Ruedas:
*     Diámetro de llanta: 66 mm.
*     Ancho de llanta: 26 mm.


***



## Como probar el programa
### 1. clonar el repositorio
Una vez que se tenga instalado Git en nuestra computadora, se debe:

* Crear una carpeta o un directorio donde se desee clonar el repositorio en nuestra computadora, Abrir la terminal desde la ruta actual e iniciar un nuevo repositorio con el siguiente comando
```
$ git init

```

* clonar el repositorio ejecutando el siguiente comando
```
$ git clone https://JCerver@bitbucket.org/JCerver/robotmilitar.git

```
Y es todo ya tendrás clonado el repositorio en tu directorio.

***


### 2. Abrir archivo .ino desde el IDE arduino 
#### Programa que debe cargarse al modulo de Wifi
Desde el IDE de arduino abrir el archivo MainESP8266.ino, es importante que el archivo este contenido en una carpeta con el mismo nombre ya que arduino no lo abrirá si no esta en esta forma, en este caso el archivo ya se encuentra dentro de la carpeta "Arduino/MainESP8266",
este programa debe de cargarse en el modulo de wifi (NodeMCU)
#### Programa que debe cargarse al Arduino UNO
Desde el IDE de arduino abrir el archivo Main.ino, es importante que el archivo este contenido en una carpeta con el mismo nombre ya que arduino no lo abrirá si no esta en esta forma, en este caso el archivo ya se encuentra dentro de la carpeta "Arduino/Main"
Listo una vez echo esto ya se tiene cargado el programa necesario en los dos componentes a utilizar

### 3. Importar librerías necesarias en Arduino IDE
#### Es necesario importar las liberías que se muestran en la siguiente lista, todas están en el gestor de librerías del IDE para descargar:


* \#include <SoftwareSerial.h>
* \#include <DHT_U.h>
* \#include <DHT.h> 


#### Libreria SoftwareSerial.h
Para esta práctica fue necesario utilizar 3 sensores, asi como motores, el modulo de wifi es el encargado de controlar a los motores pero a falta de pines para los sensores es necesario usar el arduino UNO, como los sensores están conectados al arduino UNO es necesario que este
se comunique con el modulo de Wifi porque este último es el que se encarga de comunicarse a travez de la red con la pagina web, por esta razon es necesario usar comunicacion serial entre el Arduino UNO y el modulo de Wifi, a falta de pines en el modulo de wifi se utilizo dos pines GPIO los cuales gracias a la libreria SoftwareSerial nos permite usar dos pines digitales como si fueran pines de comunicación serial es decir pin RXy pin TX. Esta libreria es usada en ambos el arduino UNO y el módulo de Wifi y los pines indicados como seriales gracias a esta libreria sirven para conectar directamente al modulo de wifi y al Arduino UNO

#### Libreria DHT.h
La librería DHT es la que permite la comunicación con los sensores DHT11 y DHT22



### 4. Cargar el programa a tu arduino
Como se menciono anteriormete es necesario cargar los siguientes archivos por separado al Arduino UNO y al módulo de Wifi:
#### Programa que debe cargarse al modulo de Wifi
En el punto 2 se explico como abrir el archivo MainESP8266.ino, una vez abierto y conectado el modulo de Wifi es necesario subir el programa al Módulo de Wifi NodeMCU

#### Programa que debe cargarse al Arduino UNO
En el punto 2 se explico como abrir el archivo Main.ino, una vez abierto y conectado el arduino UNO es necesario subir el programa.

### 5. Instalar XAMPP

#### Paso 1. Descargar el instalador de XAMPP
Visita la url [Apache Friends](https://www.apachefriends.org/download.html) y ve a la sección de Linux. Hay múltiples versiones disponibles, la diferencia es principalmente la versión de PHP. Selecciona cualquier versión de tu preferencia.

#### Paso 2. Ejecutar el instalador
Para ejecutar el instalador, primero necesitamos darle permisos de ejecución. Abre el terminal y ahí ve hasta el directorio donde descargaste el instalador y hazlo ejecutable usando el siguiente comando (en este caso el nombre del archivo es:
xampp-linux-x64-7.2.0-0-installer.run cámbialo al nombre de tu archivo).


```
$ sudo chmod +x xampp-linux-x64-7.2.0-0-installer.run

```

Luego ejecuta el instalador escribiendo lo siguiente:

```
$ sudo ./xampp-linux-x64-7.2.0-0-installer.run

```

Deberías ver la ventana de instalación, sigue los pasos con las opciones por defecto y el instalador se hará cargo del resto.
Al final de la instalación, puedes escoger si iniciar o no XAMPP despúes de hacer click en el botón de finalizar.


#### Paso 3. Probando y activando los componentes
La ventana principal de XAMPP cuenta con un panel de control del las aplicaciones del servidor, donde es posible iniciarlas, pararlas e inclusive configurarlas. Ve al tab de manejo de servidores (manage servers) para verificar el estado de las aplicaciones.
Ahora iniciemos el servidor apache y MySql.

![Imagen no disponible](Imagenes/xampp.PNG)

#### Paso 4. Probando el servicio de APACHE
Abre el navegador web y ve a http://localhost/ debería cargar el website por defecto de XAMPP, lo que significa que nuestro servidor apache esta funcionando.

![Imagen no disponible](Imagenes/localhost.png)	

### 6. Abrir archivo con la interfaz web
Del repositorio clonado existe un archivo en la ruta Web/esp8266 bajo el nombre de index.php, debemos abrir este achivo para visualizar el panel de control del sistena.

![Imagen no disponible](Imagenes/interfaz.png)	
***

## Diagrama de la RED
La idea es tener un router que sirva como punto de acceso, El nodeMCU controlara los movimientos del carro, este a su vez se comicara por wifi a trzvez del router a la pagina WEB alojada en la laptop que de la misma forma se encuentra conectada a la misma red que el node MCU. La pagina web tiene botones para controlar el vehiculo remotante y a su vez campos de texto para recibir las estadisticas de los 3 sensores que tiene el vehiculo.
Esquema de la conexión en RED:

![Red](Imagenes/Red.PNG)

***

## Diagrama del circuito.

Diagrama de circuito:

![Diagrama](Imagenes/Diagrama.png)

***
## Vista del circuito armado:
Este es el resultado al armar el circuito mostrado en el diagrama de arriba

####Vista frontal:
![Imagen no disponible](Imagenes/vista_frontal.jpg)	

####Vista lateral izquierda:
![Imagen no disponible](Imagenes/vista_lateral_izq.jpg)	

####Vista lateral derecha:
![Imagen no disponible](Imagenes/vista_lateral_der2.jpg)	

***

## Vista de la interfaz WEB

![Imagen no disponible](Imagenes/interfaz.png)


## Vista de la interfaz correspondiente a la aplicación Android

![Imagen no disponible](Imagenes/app.jpg)

***

## Video del Vehiculo Inalambrico en funcionamiento
El vehiculo se probo durante la noche, para probar la funcionalidad total, a continuacion se muestra el resultado de la prueba:
[Vehiculo en funcionamiento](https://drive.google.com/open?id=1h7APRna1wUdciloWR2qmOmN5mSJZaRNb)

***

# Conceptos técnicos
Si quieres saber como funciona el programa es necesario conocer algo de teoria para conocer que existe detrás de la magia:

***

## Uso de protocolos y puertos
### ¿Qué es el Modelo TCP/IP?
El modelo estándar para diseñar una arquitectura de red es el modelo OSI (Open Systems Interconnection), prototipo que consiste de siete capas:

* Capa física
* Enlace de datos
* Red
* Transporte
* Sesión
* Presentación
* Aplicación
 

Por su lado, el modelo TCP/IP es un prototipo híbrido derivado del OSI. El TCP/IP combina las tres capas superiores (Aplicación, Presentación y Sesión) del OSI 
en una capa (Aplicación), así mismo mantiene la capa cuatro (Transporte), combina las capas tres y dos (Red y Enlace de Datos) en una sola a la que llama Internet y mantiene la capa Física.

### Protocolo IP
El Protocolo de Internet es un protocolo de capa de red (Capa 3) diseñado en 1981 para usarse en sistemas interconectados de redes de comunicación computacional de conmutación de paquetes. 
El Protocolo de Internet y el Protocolo de Control de Transmisión (TCP, Transmission Control Protocol) son la base de los protocolos de Internet. El IP tiene dos funciones principales:

- Entrega de datagramas a través de la interred en la modalidad de mejor esfuerzo

- Fragmentación y reensamblado de datagramas

Se considera al IP un protocolo de “mejor esfuerzo”, ya que no garantiza que un paquete transmitido realmente llegue al destino ni que los datagramas transmitidos sean recibidos en el orden en que fueron enviados.
La función principal de IP es llevar paquetes de datos de un nodo fuente a un nodo destino. Este proceso se logra identificando cada paquete enviado con una dirección numérica llamada dirección IP.
El protocolo IP no tiene mecanismos de confiabilidad (RFC 791) a diferencia de los demás protocolos. En vez de tener dichos medios, este protocolo no hace uso de ellos para que sean implementados por protocolos de capa superior. 
El único mecanismo de detección de errores es la suma de verificación para el encabezado IP. Si el procedimiento de la suma de verificación falla, el datagrama será descartado y con ello no será entregado a un protocolo de nivel superior.

## Protocolo HTTP y método GET
### Protocolo HTTP
El Protocolo de Transferencia de HiperTexto (Hypertext Transfer Protocol) es un sencillo protocolo cliente-servidor que articula los intercambios de información entre los clientes Web y los servidores HTTP. 
La especificación completa del protocolo HTTP 1/0 está recogida en el RFC 1945. Fue propuesto por Tim Berners-Lee, atendiendo a las necesidades de un sistema global de distribución de información como el World Wide Web.
Desde el punto de vista de las comunicaciones, está soportado sobre los servicios de conexión TCP/IP, y funciona de la misma forma que el resto de los servicios comunes de los entornos UNIX: un proceso servidor 
escucha en un puerto de comunicaciones TCP (por defecto, el 80), y espera las solicitudes de conexión de los clientes Web. Una vez que se establece la conexión, el protocolo TCP se encarga de mantener 
la comunicación y garantizar un intercambio de datos libre de errores.

HTTP se basa en sencillas operaciones de solicitud/respuesta. Un cliente establece una conexión con un servidor y envía un mensaje con los datos de la solicitud. 
El servidor responde con un mensaje similar, que contiene el estado de la operación y su posible resultado. Todas las operaciones pueden adjuntar un objeto o recurso sobre el que actúan; 
cada objeto Web (documento HTML, fichero multimedia o aplicación CGI) es conocido por su URL.

### Método GET
El método GET envía la información codificada del usuario en el header del HTTP request, directamente en la URL. La página web y la información codificada se separan por un interrogante ?:

* El método GET envía la información en la propia URL, estando limitada a 2000 caracteres.
* La información es visible por lo que con este método nunca se envía información sensible.
* No se pueden enviar datos binarios (archivos, imágenes...).
* En PHP los datos se administran con el array asociativo $_GET.

### Protocolo DHCP
DHCP (Dynamic Host Configuration Protocol, protocolo de configuración de host dinámico) es un protocolo que permite que un equipo conectado a una red pueda obtener su configuración 
(principalmente, su configuración de red) en forma dinámica (es decir, sin una intervención especial). Solo tienes que especificarle al equipo, mediante DHCP, que encuentre una dirección IP de manera independiente. 
El objetivo principal es simplificar la administración de la red.


El protocolo DHCP sirve principalmente para distribuir direcciones IP en una red, pero desde sus inicios se diseñó como un complemento del protocolo BOOTP (Protocolo Bootstrap), que se utiliza, por ejemplo, 
cuando se instala un equipo a través de una red (BOOTP se usa junto con un servidor TFTP donde el cliente encontrará los archivos que se cargarán y copiarán en el disco duro). 
Un servidor DHCP puede devolver parámetros BOOTP o la configuración específica a un determinado host. 


## Modulo WIFI NodeMCU
### ¿Qué es?
El NodeMcu es un kit de desarrollo de código abierto basado en el popular chip ESP8266 (ESP-12E), que utiliza el lenguaje de programación Lua para crear un ambiente de desarrollo propicio para aplicaciones
que requiera conectividad Wifi de manera rápida.El ESP8266 es un chip altamente integrado diseñado para las necesidades de un nuevo mundo conectado. Ofrece una solución completa y autónoma de redes Wi-Fi, 
lo que le permite alojar la aplicación o servir como puente entre Internet y un microcontrolador.

### ¿Cómo funciona?
Esta placa viene cargada con el firmware NodeMCU, sin embargo también puede usarse como una excelente plataforma para desarrollar, evaluar y experimentar otros firmwares para el ESP8266.
El ESP8266 tiene potentes capacidades a bordo de procesamiento y almacenamiento que le permiten integrarse con sensores y dispositivos específicos de aplicación a través de sus GPIOs con un desarrollo mínimo y 
carga mínima durante el tiempo de ejecución. Su alto grado de integración en el chip permite una circuitería externa mínima, y la totalidad de la solución, incluyendo el módulo está diseñado para ocupar el área mínima en un PCB.

### Características de NodeMCU con ESP8266.

* Procesador principal: ESP8266
* Protocolo inalámbrico 802.11 b/g/n
* Stack TCP/IP integrado
* Potencia de salida +19.5dBm en modo 802.11b
* Sensor de temperatura integrado
* Corriente en espera: < 10uA

####Vista de la placa:
![Imagen no disponible](Imagenes/NodeMCU.jpg)	

Para mas información consultar la fuente:
[NodeMCU API Instrucciones](https://github.com/nodemcu/nodemcu-firmware/wiki/nodemcu_api_es)

***

***

## Uso de protocolos y puertos
### ¿Qué es el Modelo TCP/IP?
El modelo estándar para diseñar una arquitectura de red es el modelo OSI (Open Systems Interconnection), prototipo que consiste de siete capas:

* Capa física
* Enlace de datos
* Red
* Transporte
* Sesión
* Presentación
* Aplicación
 

Por su lado, el modelo TCP/IP es un prototipo híbrido derivado del OSI. El TCP/IP combina las tres capas superiores (Aplicación, Presentación y Sesión) del OSI 
en una capa (Aplicación), así mismo mantiene la capa cuatro (Transporte), combina las capas tres y dos (Red y Enlace de Datos) en una sola a la que llama Internet y mantiene la capa Física.

### Protocolo IP
El Protocolo de Internet es un protocolo de capa de red (Capa 3) diseñado en 1981 para usarse en sistemas interconectados de redes de comunicación computacional de conmutación de paquetes. 
El Protocolo de Internet y el Protocolo de Control de Transmisión (TCP, Transmission Control Protocol) son la base de los protocolos de Internet. El IP tiene dos funciones principales:

- Entrega de datagramas a través de la interred en la modalidad de mejor esfuerzo

- Fragmentación y reensamblado de datagramas

Se considera al IP un protocolo de “mejor esfuerzo”, ya que no garantiza que un paquete transmitido realmente llegue al destino ni que los datagramas transmitidos sean recibidos en el orden en que fueron enviados.
La función principal de IP es llevar paquetes de datos de un nodo fuente a un nodo destino. Este proceso se logra identificando cada paquete enviado con una dirección numérica llamada dirección IP.
El protocolo IP no tiene mecanismos de confiabilidad (RFC 791) a diferencia de los demás protocolos. En vez de tener dichos medios, este protocolo no hace uso de ellos para que sean implementados por protocolos de capa superior. 
El único mecanismo de detección de errores es la suma de verificación para el encabezado IP. Si el procedimiento de la suma de verificación falla, el datagrama será descartado y con ello no será entregado a un protocolo de nivel superior.


## Medidor de temperatura y humedad con Sensor DHT11 / DHT22
### ¿Qué es?
El DHT11 y el DHT22 son dos modelos de una misma familia de sensores, que permiten realizar la medición simultánea de temperatura y humedad.
Estos sensores disponen de un procesador interno que realiza el proceso de medición, proporcionando la medición mediante una señal digital, por lo que resulta muy sencillo obtener la medición desde un microprocesador como Arduino.
Ambos sensores presentan un encapsulado de plástico similar. Podemos distinguir ambos modelos por el color del mismo. El DHT11 presenta una carcasa azul, mientras que en el caso del sensor DHT22 el exterior es blanco.

### ¿Cómo funciona?
La conexión del DH11 y el DHT22 son idénticas, ya que como hemos comentado la única diferencia entres modelos son sus prestaciones. En ambos casos, disponemos de 4 patillas, de las cuales usaremos 3, Vcc, Output y GND.
Conectar el sensor es sencillo, simplemente alimentamos desde Arduino al sensor a través de los pines GND y Vcc del mismo. Por otro lado, conectamos la salida Output a una entrada digital de Arduino. Necesitaremos poner una
resistencia de 10K entre Vcc y el Pin Output.

El esquema eléctrico queda como la siguiente imagen:

![Imagen no disponible](Imagenes/arduinodht11.png)	

### Las características del DHT11 son realmente escasas, especialmente en rango de medición y precisión.

* Medición de temperatura entre 0 a 50, con una precisión de 2ºC
* Medición de humedad entre 20 a 80%, con precisión del 5%.
* Frecuencia de muestreo de 1 muestras por segundo (1 Hz)
* El DHT11 es un sensor muy limitado que podemos usar con fines de formación, pruebas, o en proyectos que realmente no requieran una medición precisa.

Para mas información consultar la fuente:
[MEDIR TEMPERATURA Y HUMEDAD CON ARDUINO](https://www.luisllamas.es/arduino-dht11-dht22/)
***

***

## Medir nivel de luz con fotoresistencia LDR (GL55)
### ¿Qué es?
Un fotoresistor, o LDR (light-dependent resistor) es un dispositivo cuya resistencia varia en función de la luz recibida. Podemos usar esta variación para medir, a través de las entradas analógicas, una estimación del nivel del luz.
Un fotoresistor está formado por un semiconductor, típicamente sulfuro de cadmio CdS. Al incidir la luz sobre él algunos de los fotones son absorbidos, provocando que electrones pasen a la banda de conducción y, por tanto, 
disminuyendo la resistencia del componente.Por tanto, un fotoresistor disminuye su resistencia a medida que aumenta la luz sobre él. Los valores típicos son de 1 Mohm en total oscuridad, a 50-100 Ohm bajo luz brillante.

Por otro lado, la variación de la resistencia es relativamente lenta, de 20 a 100 ms en función del modelo. Esta lentitud hace que no sea posible registrar variaciones rápidas, como las producidas en fuentes de luz artificiales
alimentadas por corriente alterna. Este comportamiento puede ser beneficioso, ya que dota al sensor de una gran estabilidad.

### ¿Cómo funciona?
Matemáticamente, la relación entre la iluminancia y la resistencia de una LDR sigue una función potencial.
![Imagen no disponible](Imagenes%20/vista_general.jpg)	

La constante gamma es la pendiente de la gráfica logarítmica, o la pérdida de resistencia por década. Su valor típicamente 0.5 a 0.8.
Por este motivo, frecuentemente las gráficas que relacionan ambos valores se representan en escalas logarítmicas para ambos ejes. Bajo esta representación, la relación se muestra como una gráfica lineal.
Estos valores pueden ser obtenidos del datasheet del componente. Por ejemplo, para la familia GL55 de fotoresistores son los siguientes:

![Imagen no disponible](Imagenes/tabla.png)	

El esquema eléctrico sería el siguiente:

![Imagen no disponible](Imagenes/arduinoldr.png)

Para mas información consultar la fuente:
[MEDIR NIVEL DE LUZ CON ARDUINO](https://www.luisllamas.es/medir-nivel-luz-con-arduino-y-fotoresistencia-ldr/)

***

***

## Detector de obstáculos con sensor infrarrojo
### ¿Qué es?
Un detector de obstáculos infrarrojo es un dispositivo que detecta la presencia de un objeto mediante la reflexión que produce en la luz. El uso de luz infrarroja (IR) es simplemente para que esta no sea visible para los humanos.
Constitutivamente son sensores sencillos. Se dispone de un LED emisor de luz infrarroja y de un fotodiodo (tipo BPV10NF o similar) que recibe la luz reflejada por un posible obstáculo.

Los detectores de obstáculo suelen proporcionarse con una placa de medición estándar con el comparador LM393, que permite obtener la lectura como un valor digital cuando se supera un cierto umbral, que se regula a través de un 
potenciómetro ubicado en la placa.

![Imagen no disponible](Imagenes/arduinofuncionamiento.png)

### ¿Cómo funciona?
Este tipo de sensores actúan a distancias cortas, típicamente de 5 a 20mm. Además la cantidad de luz infrarroja recibida depende del color, material, forma y posición del obstáculo, por lo que no disponen de una precisión suficiente para 
proporcionar una estimación de la distancia al obstáculo. Pese a esta limitación son ampliamente utilizados para la detección de obstáculos en pequeños vehículos o robots. Su bajo coste hace que sea frecuente ubicarlos en el perímetro,
de forma que detectemos obstáculos en varias direcciones.
También son útiles en otro tipo de aplicaciones como, por ejemplo, detectar la presencia de un objeto en una determinada zona, determinar una puerta está abierta o cerrada, o si una máquina ha alcanzado un cierto punto en su desplazamiento.
El montaje es sencillo. Alimentamos el módulo a través de Vcc y GND conectándolos, respectivamente, a la salida de 5V y GND en Arduino.

Finalmente, conectamos la salida digital del sensor a una entrada digital para leer el estado del sensor.

![No_disponible](Imagenes/obstaculosesquema.png)


Para mas información consultar la fuente:
[DETECTOR DE OBSTÁCULOS CON SENSOR INFRARROJO Y ARDUINO](https://www.luisllamas.es/detectar-obstaculos-con-sensor-infrarrojo-y-arduino/)

***

***
## Paquete XAMPP
### ¿Qué es?
XAMPP es un paquete de software libre, que consiste principalmente en el sistema de gestión de bases de datos MySQL, el servidor web Apache y los intérpretes para lenguajes de script PHP y Perl. El nombre es en realidad un acrónimo: X 
(para cualquiera de los diferentes sistemas operativos), Apache, MariaDB/MySQL, PHP, Perl. A partir de la versión 5.6.15, XAMPP cambió la base de datos MySQL por MariaDB, un fork de MySQL con licencia GPL.
El programa se distribuye con la licencia GNU y actúa como un servidor web libre, fácil de usar y capaz de interpretar páginas dinámicas. A esta fecha, XAMPP está disponible para Microsoft Windows, GNU/Linux, Solaris y Mac OS X.

### ¿Cómo funciona?
XAMPP ofrece los siguientes servicio

* Apache: el servidor web de código abierto es la aplicación más usada globalmente para la entrega de contenidos web. Las aplicaciones del servidor son ofrecidas como software libre por la Apache Software Foundation.
* MySQL/MariaDB: conMySQL, XAMPP cuenta con uno de los sistemas relacionales de gestión de bases de datos más populares del mundo. En combinación con el servidor web Apache y el lenguaje PHP, MySQL sirve para el almacenamiento de datos para servicios web. En las versiones actuales de XAMPP esta base 
  de datos se ha sustituido por MariaDB, una ramificación (“Fork”) del proyecto MySQL.
* PHP: es un lenguaje de programación de código de lado del servidor que permite crear páginas web o aplicaciones dinámicas. Es independiente de plataforma y soporta varios sistemas de bases de datos.
* Perl: este lenguaje de programación se usa en la administración del sistema, en el desarrollo web y en la programación de red. También permite programar aplicaciones web dinámicas.


***



# Autores ✒️
* **José Guadalupe de Jesús Cervera Barbosa** - *Trabajo general* - 
* **Miguel Ángel Ramírez Lira** - *Trabajo general* - 
* **Alfredo Valivia Barajas** -*Trabajo general* - 



